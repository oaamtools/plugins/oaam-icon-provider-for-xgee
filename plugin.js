export async function init(pluginAPI) {
  var iconProvider = pluginAPI.require("iconProvider");
  if (!pluginAPI.getInterface("iconProvider.providers")) {
    throw "INTERFACE DEFINITION MISSING";
  }
  class OAAMIconProvider extends pluginAPI.getInterface("iconProvider.providers") {
    constructor() {
      super();
    }
    isApplicable(eObject) {
      if (eObject.eClass) {
        switch (eObject.eClass.get("name")) {
          case "EClass":
            if (eObject.eContainer) {
              if (eObject.eContainer.get("nsURI").includes("http://www.oaam.de/oaam/model/v160")) {
                return true;
              }
            }
            break;
          default:
            if (eObject.eClass.eContainer) {
              if (
                eObject.eClass.eContainer
                  .get("nsURI")
                  .includes("http://www.oaam.de/oaam/model/v160")
              ) {
                return true;
              }
            }
        }
      }
      return false;
    }

    getPathToIcon(eObject) {
      if (eObject.eClass) {
        switch (eObject.eClass.get("name")) {
          case "EReference":
            return null;
          case "EAttribute":
            return null;
          case "EClass":
            if (eObject.eContainer) {
              if (eObject.eContainer.get("nsURI").includes("http://www.oaam.de/oaam/model/v160")) {
                return "./" + pluginAPI.getPath() + "icons/objects/" + eObject.get("name") + ".gif";
              }
            }
            break;
          default:
            if (eObject.eClass.eContainer) {
              if (
                eObject.eClass.eContainer
                  .get("nsURI")
                  .includes("http://www.oaam.de/oaam/model/v160")
              ) {
                return pluginAPI.getPath() + "happyIcon";
              }
            }
        }
      }
      return null;
    }
  }

  pluginAPI.implement("iconProvider.providers", new OAAMIconProvider());
}

export var meta = {
  id: "iconProvider.oaam",
  description: "IconProvider for OAAM",
  author: "Matthias Brunner",
  version: "0.1.0",
  requires: ["iconProvider"],
};
